package techu.practitioner.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.modelo.Proveedor;
import techu.practitioner.servicio.ServicioProveedor;

import java.util.Arrays;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(RutasAPI.PROVEEDOR)
@CrossOrigin
public class ControladorProveedor {

    @Autowired
    ServicioProveedor servicioProveedor;

    @GetMapping
    public EntityModel<Proveedor> obtenerProveedor(@PathVariable(name = "idProveedor") String id) {
        final Proveedor p = buscarProveedorPorId(id);
        return EntityModel.of(p).add(Arrays.asList(
            linkTo(methodOn(ControladorProveedor.class).obtenerProveedor(p.id))
                .withSelfRel(),
            linkTo(methodOn(ControladorListaProveedores.class).obtenerProveedores())
               .withRel("proveedores")
               .withTitle("Ver todos los proveedores")
        ));
    }

    @PutMapping
    public void reemplazarProveedor(@PathVariable(name = "idProveedor") String id,
                                   @RequestBody ProveedorEntrada p) {
        final Proveedor x = buscarProveedorPorId(id); // Si no existe, lanza excepcion 404.

        if(p.nombre == null || p.nombre.trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(p.direccion == null || p.direccion.trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        x.nombre = p.nombre;
        x.direccion = p.direccion;

        this.servicioProveedor.reemplazarProveedor(id, x);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarProveedor(@PathVariable(name = "idProveedor") String id) {
        this.servicioProveedor.borrarProveedor(id);
    }

    @PatchMapping
    public void modificarProveedor(@PathVariable(name = "idProveedor") String id,
                                  @RequestBody ProveedorEntrada p) {
        final Proveedor x = buscarProveedorPorId(id);
        if(p.nombre != null) {
            if(p.nombre.trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.nombre = p.nombre;
        }
        if(p.direccion != null) {
            if(p.direccion.trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.direccion = p.direccion;
        }

        this.servicioProveedor.reemplazarProveedor(id, x);
    }

    private Proveedor buscarProveedorPorId(String idProveedor) {
        final Proveedor p = this.servicioProveedor.obtenerProveedor(idProveedor);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }

    public static class ProveedorEntrada {

        public String nombre;
        public String direccion;
    }
}
