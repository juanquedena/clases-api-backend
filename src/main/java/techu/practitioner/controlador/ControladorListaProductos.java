package techu.practitioner.controlador;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.jose4j.jwt.JwtClaims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.modelo.Producto;
import techu.practitioner.seguridad.JwtBuilder;
import techu.practitioner.servicio.ServicioProducto;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(RutasAPI.PRODUCTOS)
@CrossOrigin
public class ControladorListaProductos {

    @Autowired
    ServicioProducto servicioProducto;

    @Autowired
    JwtBuilder jwtBuilder;

    @PostMapping
    public ResponseEntity agregarProducto(@RequestBody Producto p) {
        this.servicioProducto.agregar(p);
        return ResponseEntity
                .ok()
                .location(obtenerUrlProducto(p).toUri())
                .build();
    }

    @GetMapping
    public CollectionModel<EntityModel<ResumenProducto>> obtenerProductos(
            @RequestHeader("Authorization") String authorization,
            @RequestParam(required = false) Double minPrecio,
            @RequestParam(required = false) Double maxPrecio
    ) {
        validarToken(authorization);
        List<Producto> pp = null;

        if(minPrecio != null && maxPrecio != null) {
            pp = this.servicioProducto.obtenerPorRangoPrecio(minPrecio, maxPrecio);
        } else {
            pp = this.servicioProducto.obtenerProductos();
        }

        return CollectionModel.of(
                pp.stream()
                        .map(x -> crearRespuestaResumenProducto(x))
                        .collect(Collectors.toList()))
                .add(linkTo(methodOn(this.getClass())
                        .obtenerProductos(null, null, null)).withSelfRel());
    }

    private EntityModel<ResumenProducto> crearRespuestaResumenProducto(Producto p) {
        final ResumenProducto r = new ResumenProducto();
        r.nombre = p.nombre;
        r.precio = p.precio;
        return EntityModel.of(r).add(obtenerUrlProducto(p));
    }

    private Link obtenerUrlProducto(Producto p) {
        return linkTo(methodOn(ControladorProducto.class).obtenerProducto(p.id))
                .withSelfRel()
                .withTitle("Ver detalles de este producto");
    }

    private void validarToken(String authorization) {

        // Sanear la entrada.
        authorization = authorization == null ? "" : authorization.trim();

        // TODO: validar el token.
        System.out.println("Authorization: " + authorization);

        if(!authorization.startsWith("Bearer "))
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);

        final String token = authorization.substring(7).trim();


        try {
            final JwtClaims claims = jwtBuilder.generateParseToken(token);

            // JWT - JSON Web Token {}
            if (!claims.hasClaim("userID"))
                throw new ResponseStatusException(HttpStatus.FORBIDDEN);

            System.out.println("AUTENTICACION: userID = " + claims.getClaimValue("userID"));

        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    public static class ResumenProducto {

        public String nombre;
        public Double precio;
    }
}
