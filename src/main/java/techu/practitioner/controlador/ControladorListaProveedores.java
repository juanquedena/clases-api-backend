package techu.practitioner.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import techu.practitioner.modelo.Proveedor;
import techu.practitioner.servicio.ServicioProveedor;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(RutasAPI.PROVEEDORES)
@CrossOrigin
public class ControladorListaProveedores {

    @Autowired
    ServicioProveedor servicioProveedor;

    @PostMapping
    public ResponseEntity agregarProveedor(@RequestBody Proveedor p) {
        this.servicioProveedor.agregar(p);
        return ResponseEntity
                .ok()
                .location(obtenerUrlProveedor(p).toUri())
                .build();
    }

    @GetMapping
    public CollectionModel<EntityModel<Proveedor>> obtenerProveedores() {
        List<Proveedor> pp = this.servicioProveedor.obtenerProveedores();
        return CollectionModel.of(
                pp.stream()
                        .map(x -> crearRespuestaProveedor(x))
                        .collect(Collectors.toList()))
                .add(linkTo(methodOn(this.getClass()).obtenerProveedores()).withSelfRel());
    }

    private Link obtenerUrlProveedor(Proveedor p) {
        return linkTo(methodOn(ControladorProveedor.class).obtenerProveedor(p.id))
                .withSelfRel()
                .withTitle("Ver detalles de este proveedor");
    }

    private EntityModel<Proveedor> crearRespuestaProveedor(Proveedor p) {
        return EntityModel.of(p).add(Arrays.asList(
                obtenerUrlProveedor(p),
                linkTo(methodOn(this.getClass()).obtenerProveedores())
                        .withRel("proveedores")
                        .withTitle("Ver todos los proveedor")
        ));
    }
}
