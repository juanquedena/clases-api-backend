package techu.practitioner.controlador;

public class RutasAPI {

    public static final String PRODUCTOS              = "/stocks/v1/productos";
    public static final String PRODUCTO               = "/stocks/v1/productos/{idProducto}";
    public static final String PROVEEDORES_PRODUCTO   = "/stocks/v1/productos/{idProducto}/proveedores";
    public static final String PROVEEDORES            = "/stocks/v1/proveedores";
    public static final String PROVEEDOR              = "/stocks/v1/proveedores/{idProveedor}";
    public static final String PEDIDOS_PROVEEDOR      = "/stocks/v1/proveedores/{idProveedor}/pedidos";

    public static final String AUTENTICACION          = "/autent/v1/token";
}
