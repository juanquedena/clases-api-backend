package techu.practitioner.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import techu.practitioner.modelo.DatosPedido;
import techu.practitioner.servicio.ServicioProveedor;

import java.util.List;

@RestController
@RequestMapping("/stocks/v1/pedidos")
public class ControladorPedidos {

    @Autowired
    ServicioProveedor servicioProveedor;


    @GetMapping
    public List<DatosPedido> obtenerPedidos(
            @RequestParam(required = false) String producto,
            @RequestParam(required = false) Integer estado) {
        return this.servicioProveedor.obtenerPedidos(producto, estado);
    }

}