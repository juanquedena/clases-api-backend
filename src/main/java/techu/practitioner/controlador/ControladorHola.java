package techu.practitioner.controlador;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/saludo")
@CrossOrigin
public class ControladorHola {

    private String saludo = "Hola";

    @GetMapping
    public String saludar(@RequestParam(required = false, name = "persona") String nombre) {
        if(nombre == null || nombre.trim().isEmpty())
            return String.format("¡%s, mundo!", this.saludo);
        return String.format("¡%s, %s!", this.saludo, nombre);
    }

    @PutMapping
    public ResponseEntity guardarNuevoSaludo(@RequestBody String saludoNuevo) {
        if(saludoNuevo != null && !saludoNuevo.trim().isEmpty()) {
            this.saludo = saludoNuevo;
            return ResponseEntity.ok("Se guardó el saludo: " + this.saludo);
        } else {
            return ResponseEntity
                    .status(403)
                    .body("El saludo no puede estar vacío");
        }
    }

}
