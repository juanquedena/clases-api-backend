package techu.practitioner.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.modelo.Producto;
import techu.practitioner.modelo.Proveedor;
import techu.practitioner.servicio.ServicioProducto;
import techu.practitioner.servicio.ServicioProveedor;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(RutasAPI.PROVEEDORES_PRODUCTO)
@CrossOrigin
public class ControladorProveedoresProducto {

    @Autowired
    ServicioProducto servicioProducto;

    @Autowired
    ServicioProveedor servicioProveedor;

    @GetMapping
    public CollectionModel<EntityModel<ProveedorProducto>> obtenerProveedoresProducto(@PathVariable(name = "idProducto") String id) {
        final Producto p = this.buscarProductoPorId(id);
        final List<String> idProveedores = p.idProveedores;

        return CollectionModel.of(idProveedores.stream().map(
                idProveedor -> EntityModel
                        .of(new ProveedorProducto())
                        .add(linkTo(methodOn(ControladorProveedor.class).obtenerProveedor(idProveedor))
                                .withRel("proveedor")
                                .withTitle("Proveedor de este producto"))
        ).collect(Collectors.toUnmodifiableList())).add(Arrays.asList(
            linkTo(methodOn(this.getClass()).obtenerProveedoresProducto(id)).withSelfRel()
        ));
    }

    @PutMapping
    public void agregarReferenciaProveedor(@PathVariable(name = "idProducto") String id,
                                           @RequestBody ProveedorEntrada p) {
        final Producto x = this.buscarProductoPorId(id);
        final Proveedor v = this.servicioProveedor.obtenerProveedor(p.idProveedor);
        if(v == null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        for(String z : x.idProveedores) {
            if(z.equalsIgnoreCase(p.idProveedor))
                return;
        }

        x.idProveedores.add(p.idProveedor);
        this.servicioProducto.reemplazarProducto(id, x);
    }

    @DeleteMapping("/{idProveedor}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarReferenciaProveedor(@PathVariable(name = "idProducto") String id,
                                          @PathVariable String idProveedor) {
        final Producto x = this.buscarProductoPorId(id);
        boolean encontrado = false;
        int j = 0;
        for(int i = 0; i < x.idProveedores.size(); i++) {
            if(x.idProveedores.get(i).equalsIgnoreCase(idProveedor)) {
                j = i;
                encontrado = true;
            }
        }
        if(encontrado) {
            x.idProveedores.remove(j);
            this.servicioProducto.reemplazarProducto(id, x);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    private Producto buscarProductoPorId(String idProducto) {
        final Producto p = this.servicioProducto.obtenerProducto(idProducto);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }

    public static class ProveedorEntrada {
        public String idProveedor;
    }

    public static class ProveedorProducto {

        public String nombre;
    }
}
