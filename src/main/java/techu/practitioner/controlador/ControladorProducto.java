package techu.practitioner.controlador;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.modelo.Producto;
import techu.practitioner.servicio.ServicioProducto;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(RutasAPI.PRODUCTO)
@CrossOrigin
public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;

    @GetMapping
    public EntityModel<Producto> obtenerProducto(@PathVariable(name = "idProducto") String id) {
        return crearRespuestaProducto(buscarProductoPorId(id));
    }

    @PutMapping
    public void reemplazarProducto(@PathVariable(name = "idProducto") String id,
                                   @RequestBody ProductoEntrada p) {
        final Producto x =buscarProductoPorId(id); // Si no existe, lanza excepcion 404.

        if(p.nombre == null || p.nombre.trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(p.precio == null || p.precio < 0.0)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(p.cantidad == null || p.cantidad < 0.0)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        x.nombre = p.nombre;
        x.precio = p.precio;
        x.cantidad = p.cantidad;

        this.servicioProducto.reemplazarProducto(id, x);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarProducto(@PathVariable(name = "idProducto") String id) {
        this.servicioProducto.borrarProducto(id);
    }

    @PatchMapping
    public void modificarProducto(@PathVariable(name = "idProducto") String id,
                                  @RequestBody ProductoEntrada p) {

        // final Producto x = buscarProductoPorId(id);
        if(!this.servicioProducto.existeProducto(id))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        if(p.nombre != null) {
            if(p.nombre.trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            // x.nombre = p.nombre;
        }
        if(p.precio != null) {
            if(p.precio < 0.0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            // x.precio = p.precio;
        }
        if(p.cantidad != null) {
            if(p.cantidad < 0.0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            // x.cantidad = p.cantidad;
        }

        // this.servicioProducto.reemplazarProducto(id, x);
        this.servicioProducto.emparcharProducto(id, p.nombre, p.precio, p.cantidad);
    }

    private Producto buscarProductoPorId(String idProducto) {
        final Producto p = this.servicioProducto.obtenerProducto(idProducto);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }

    private EntityModel<Producto> crearRespuestaProducto(Producto p) {
        return EntityModel.of(p).add(crearEnlacesProducto(p));
    }

    private Link obtenerUrlProducto(Producto p) {
        return linkTo(methodOn(ControladorProducto.class).obtenerProducto(p.id))
                .withSelfRel()
                .withTitle("Ver detalles de este producto");
    }

    private List<Link> crearEnlacesProducto(Producto p) {
        final Link productos = linkTo(methodOn(ControladorListaProductos.class).obtenerProductos(null, null, null))
                .withRel("productos")
                .withTitle("Lista de todos los productos");

        final Link proveedores = linkTo(methodOn(ControladorProveedoresProducto.class).obtenerProveedoresProducto(p.id))
                .withRel("proveedores")
                .withTitle("Lista de proveedores");

        return Arrays.asList(
                obtenerUrlProducto(p),
                productos,
                proveedores
        );
    }

    public static class ProductoEntrada {

        public String nombre;
        public Double precio;
        public Double cantidad;
    }
}
