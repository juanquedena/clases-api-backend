package techu.practitioner.controlador;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.modelo.Pedido;
import techu.practitioner.modelo.Proveedor;
import techu.practitioner.servicio.ServicioProveedor;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(RutasAPI.PEDIDOS_PROVEEDOR)
@CrossOrigin
public class ControladorPedidosProveedor {

    @Autowired
    ServicioProveedor servicioProveedor;

    @GetMapping
    public CollectionModel<EntityModel<Pedido>> obtenerPedidosProveedor(@PathVariable String idProveedor) {
        final List<Pedido> pedidos = obtenerProveedorPorId(idProveedor).pedidos;

        return CollectionModel.of(
                pedidos.stream().map(
                    p -> EntityModel.of(p).add(Arrays.asList(
                        linkTo(methodOn(this.getClass()).obtenerPedidoPorId(idProveedor,p.id)).withSelfRel(),
                        linkTo(methodOn(ControladorProveedor.class).obtenerProveedor(idProveedor)).withRel("proveedor")
                    ))
                ).collect(Collectors.toUnmodifiableList()))
            .add(Arrays.asList(
                linkTo(methodOn(this.getClass())
                    .obtenerPedidosProveedor(idProveedor))
                    .withSelfRel()
            ));
    }

    @PostMapping
    public ResponseEntity agregarPedidoProveedor(@PathVariable String idProveedor,
                                                 @RequestBody Pedido pedido) {
        final Proveedor p = obtenerProveedorPorId(idProveedor);
        final List<Pedido> pedidos = p.pedidos;
        pedidos.add(pedido);
        pedido.id = pedidos.size();
        this.servicioProveedor.reemplazarProveedor(idProveedor, p);

        final Link enlacePedido = linkTo(methodOn(this.getClass())
                .obtenerPedidoPorId(idProveedor, pedido.id))
                .withSelfRel();

        return ResponseEntity.ok()
                .location(enlacePedido.toUri())
                .build();
    }

    @GetMapping("/{idPedido}")
    public Pedido obtenerPedidoPorId(@PathVariable String idProveedor, @PathVariable long idPedido) {
        try {
            final List<Pedido> pedidos = obtenerProveedorPorId(idProveedor).pedidos;
            return pedidos.get((int) idPedido - 1);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{idPedido}")
    public void cambiarEstadoPedido(@PathVariable String idProveedor,
                                    @PathVariable long idPedido,
                                    @RequestBody EstadoPedido nuevoEstado) {
        try {
            final Proveedor proveedor = obtenerProveedorPorId(idProveedor);
            final List<Pedido> pedidos = proveedor.pedidos;
            final Pedido p = pedidos.get((int) idPedido - 1);
            p.estado = nuevoEstado.estado;
            this.servicioProveedor.reemplazarProveedor(idProveedor, proveedor);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    private Proveedor obtenerProveedorPorId(String idProveedor) {
        final Proveedor p = this.servicioProveedor.obtenerProveedor(idProveedor);
        if (p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;

    }

    public static class EstadoPedido {
        public int estado;
    }
}
