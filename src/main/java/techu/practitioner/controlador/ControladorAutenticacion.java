package techu.practitioner.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.seguridad.JwtBuilder;

@RestController
@RequestMapping(RutasAPI.AUTENTICACION)
public class ControladorAutenticacion {

    public static class Aut {
        public String userId;
        public String passwd;
    }

    @Autowired
    JwtBuilder jwtBuilder;

    @PostMapping
    public String obtenerToken(@RequestBody Aut aut) {
        if(!"desarrollo".equalsIgnoreCase(aut.userId)
                || !"password1234".equalsIgnoreCase(aut.passwd)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }

        return jwtBuilder.generateToken(aut.userId, "ROL_PRUEBA");
    }
}
