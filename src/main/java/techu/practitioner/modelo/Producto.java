package techu.practitioner.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class Producto {

    @JsonIgnore
    public String id;
    public String nombre;
    public double precio;
    public double cantidad;

    @JsonIgnore
    public List<String> idProveedores = new ArrayList<>();
}
