package techu.practitioner.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Pedido {

    @JsonIgnore
    public long id;
    public String producto;
    public double cantidad;
    public int estado;
}
