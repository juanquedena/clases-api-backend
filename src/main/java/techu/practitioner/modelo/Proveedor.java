package techu.practitioner.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class Proveedor {

    @JsonIgnore
    public String id;
    public String nombre;
    public String direccion;

    @JsonIgnore
    public List<Pedido> pedidos = new ArrayList<>();
}
