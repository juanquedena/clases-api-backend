package techu.practitioner.servicio.impl;

import org.springframework.stereotype.Service;
import techu.practitioner.modelo.DatosPedido;
import techu.practitioner.modelo.Proveedor;
import techu.practitioner.servicio.ServicioProveedor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class ServicioProveedorMemoriaImpl implements ServicioProveedor {

    private final AtomicLong secuenciador = new AtomicLong(0);
    private final ConcurrentHashMap<String, Proveedor> proveedores = new ConcurrentHashMap<>();

    @Override
    public String agregar(Proveedor p) {
        p.id = String.format("%08d", this.secuenciador.incrementAndGet());
        this.proveedores.put(p.id, p);
        return p.id;
    }

    @Override
    public List<Proveedor> obtenerProveedores() {
        return this.proveedores
                .entrySet()
                .stream()
                .map(x -> x.getValue())
                .collect(Collectors.toList());
    }

    @Override
    public Proveedor obtenerProveedor(String id) {
        final Proveedor p = this.proveedores.get(id);

        return p;
    }

    @Override
    public void reemplazarProveedor(String id, Proveedor p) {
        p.id = id;
        this.proveedores.put(id, p);
    }

    @Override
    public void borrarProveedor(String id) {
        this.proveedores.remove(id);
    }

    @Override
    public List<DatosPedido> obtenerPedidos(String producto, Integer estado) {
        return new ArrayList<>();
    }
}
