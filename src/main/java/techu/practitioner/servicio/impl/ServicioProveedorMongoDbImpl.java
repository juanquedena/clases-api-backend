package techu.practitioner.servicio.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import techu.practitioner.modelo.DatosPedido;
import techu.practitioner.modelo.Pedido;
import techu.practitioner.modelo.Proveedor;
import techu.practitioner.servicio.ServicioProveedor;
import techu.practitioner.servicio.repos.RepositorioProveedor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ServicioProveedorMongoDbImpl implements ServicioProveedor {

    @Autowired
    RepositorioProveedor repositorioProveedor;

    @Override
    public String agregar(Proveedor p) {
        return this.repositorioProveedor.save(p).id;
    }

    @Override
    public List<Proveedor> obtenerProveedores() {
        return this.repositorioProveedor.findAll();
    }

    @Override
    public Proveedor obtenerProveedor(String id) {
        final Optional<Proveedor> p = this.repositorioProveedor.findById(id);
        return p.isPresent() ? p.get() : null;
    }

    @Override
    public void reemplazarProveedor(String id, Proveedor p) {
        p.id = id;
        this.repositorioProveedor.save(p);
    }

    @Override
    public void borrarProveedor(String id) {
        this.repositorioProveedor.deleteById(id);
    }

    @Override
    public List<DatosPedido> obtenerPedidos(String producto, Integer estado) {
        /**
        final List<DatosPedido> resultado = new ArrayList<>();
        final List<Proveedor> proveedores = this.obtenerProveedores();

        for(Proveedor proveedor: proveedores) {
            for(Pedido pedido: proveedor.pedidos) {
                final DatosPedido datosPedido = new DatosPedido();
                datosPedido.idProveedor = proveedor.id;
                datosPedido.idPedido = pedido.id;
                datosPedido.producto = pedido.producto;
                datosPedido.proveedor = proveedor.nombre;
                datosPedido.cantidad = pedido.cantidad;
                datosPedido.estado = estado;
                resultado.add(datosPedido);
            }
        }

        return resultado;
        **/
        return this.repositorioProveedor.obtenerPedidos(producto, estado);
    }
}
