package techu.practitioner.servicio.impl;

import org.springframework.stereotype.Service;
import techu.practitioner.modelo.Producto;
import techu.practitioner.servicio.ServicioProducto;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class ServicioProductoMemoriaImpl implements ServicioProducto {

    private final AtomicLong secuenciador = new AtomicLong(0);
    private final ConcurrentHashMap<String, Producto> productos = new ConcurrentHashMap<>();

    @Override
    public String agregar(Producto p) {
        p.id = String.format("%08d", this.secuenciador.incrementAndGet());
        this.productos.put(p.id, p);
        return p.id;
    }

    @Override
    public List<Producto> obtenerProductos() {
        return this.productos
                .entrySet()
                .stream()
                .map(x -> x.getValue())
                .collect(Collectors.toList());
    }

    @Override
    public Producto obtenerProducto(String id) {
        final Producto p = this.productos.get(id);

        return p;
    }

    @Override
    public void reemplazarProducto(String id, Producto p) {
        p.id = id;
        this.productos.put(id, p);
    }

    @Override
    public void borrarProducto(String id) {
        this.productos.remove(id);
    }

    @Override
    public List<Producto> obtenerPorRangoPrecio(double minPrecio, double maxPrecio) {
        return new ArrayList<>();
    }

    @Override
    public boolean existeProducto(String idProducto) {
        return false;
    }

    @Override
    public void emparcharProducto(String idProducto, String nombre, Double precio, Double cantidad) {
    }
}
