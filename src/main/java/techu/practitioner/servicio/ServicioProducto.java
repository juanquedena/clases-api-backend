package techu.practitioner.servicio;

import techu.practitioner.modelo.Producto;

import java.util.List;

public interface ServicioProducto {

    String agregar(Producto p);

    List<Producto> obtenerProductos();

    Producto obtenerProducto(String id);

    void reemplazarProducto(String id, Producto p);

    void borrarProducto(String id);

    List<Producto> obtenerPorRangoPrecio(double minPrecio, double maxPrecio);

    boolean existeProducto(String idProducto);

    void emparcharProducto(String idProducto, String nombre, Double precio, Double cantidad);
}
