package techu.practitioner.servicio.repos;

import org.springframework.stereotype.Repository;
import techu.practitioner.modelo.Producto;

import java.util.List;

@Repository
public interface RepositorioProductoPersonalizado {

    List<Producto> buscarPorRangoPrecioComplicado(double min, double max);

    void emparcharProducto(String idProducto, String nombre, Double precio, Double cantidad);

    boolean existeProducto(String idProducto);
}