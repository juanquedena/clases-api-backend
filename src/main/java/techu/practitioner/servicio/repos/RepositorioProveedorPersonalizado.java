package techu.practitioner.servicio.repos;

import org.springframework.stereotype.Repository;
import techu.practitioner.modelo.DatosPedido;

import java.util.List;

@Repository
public interface RepositorioProveedorPersonalizado {

    List<DatosPedido> obtenerPedidos(String producto, Integer estado);
}
