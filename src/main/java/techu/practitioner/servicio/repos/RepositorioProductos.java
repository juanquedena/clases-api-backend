package techu.practitioner.servicio.repos;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import techu.practitioner.modelo.Producto;

import java.util.List;

public interface RepositorioProductos extends MongoRepository<Producto, String>, RepositorioProductoPersonalizado {

    List<Producto> findByPrecioBetween(double min, double max);

    @Query("{  $and : [  { 'precio' : { $gte : ?0 } }  , { 'precio' : { $lte : ?1 }}     ]   }")
    List<Producto> buscarPorRangoPrecio(double min, double max);

}
