package techu.practitioner.servicio.repos.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.ObjectOperators;
import org.springframework.data.mongodb.core.query.Criteria;
import techu.practitioner.modelo.DatosPedido;
import techu.practitioner.servicio.repos.RepositorioProveedorPersonalizado;

import java.util.List;
import java.util.Map;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

public class RepositorioProveedorPersonalizadoImpl implements RepositorioProveedorPersonalizado {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<DatosPedido> obtenerPedidos(String producto, Integer estado) {
        /**
        System.out.println(String.format("===== %s.obtenerPedidos('%s', %d)", this.getClass().getName(), producto, estado));


        final UnwindOperation etapaUnwind = Aggregation.unwind("$pedidos");

        final AddFieldsOperation etapaAddFields = Aggregation.addFields()
                .addFieldWithValue("idProveedor" , "$_id")
                .addFieldWithValue("idPedido", "$pedidos._id")
                .addFieldWithValue("proveedor", "$nombre")
                .addFieldWithValue("producto", "$pedidos.producto")
                .addFieldWithValue("cantidad", "$pedidos.cantidad")
                .addFieldWithValue("estado", "$pedidos.estado")
                .build();

        final ProjectionOperation etapaProjection = Aggregation.project()
                .andInclude(
                        "idProveedor",
                        "idPedido",
                        "proveedor",
                        "producto",
                        "cantidad",
                        "estado")
                .andExclude("_id");


        final Aggregation tuberia = Aggregation.newAggregation(etapaUnwind, etapaAddFields, etapaProjection);

        final AggregationResults<DatosPedido> resultado =
                this.mongoTemplate.aggregate(tuberia, "proveedor", DatosPedido.class);

        return resultado.getMappedResults();
        **/

        /**
        return this.mongoTemplate.aggregate(
            newAggregation(
                match(Criteria.where("pedidos.producto").is(producto).and("pedidos.estado").is(estado)),
                unwind("$pedidos"),
                match(Criteria.where("pedidos.producto").is(producto).and("pedidos.estado").is(estado)),
                addFields()
                    .addFieldWithValue("idProveedor" , "$_id")
                    .addFieldWithValue("idPedido", "$pedidos._id")
                    .addFieldWithValue("proveedor", "$nombre")
                    .addFieldWithValue("producto", "$pedidos.producto")
                    .addFieldWithValue("cantidad", "$pedidos.cantidad")
                    .addFieldWithValue("estado", "$pedidos.estado")
                    .build(),
                project()
                    .andInclude(
                        "idProveedor",
                        "idPedido",
                        "proveedor",
                        "producto",
                        "cantidad",
                        "estado")
                    .andExclude("_id")
            ), "proveedor", DatosPedido.class).getMappedResults();
        **/

        return this.mongoTemplate.aggregate(
            newAggregation(
                match(Criteria.where("pedidos.producto").is(producto).and("pedidos.estado").is(estado)),
                unwind("$pedidos"),
                match(Criteria.where("pedidos.producto").is(producto).and("pedidos.estado").is(estado)),
                 replaceRoot(ObjectOperators.MergeObjects.merge(
                    // k -> v
                    Map.of("idProveedor", "$_id",
                            "proveedor", "$nombre",
                            "idPedido" , "$pedidos._id",
                            "producto" , "$pedidos.producto",
                            "cantidad" , "$pedidos.cantidad")
                ))
            ), "proveedor", DatosPedido.class).getMappedResults();
    }

}
