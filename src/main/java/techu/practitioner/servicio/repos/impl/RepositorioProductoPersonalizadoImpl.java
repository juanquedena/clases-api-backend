package techu.practitioner.servicio.repos.impl;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import techu.practitioner.modelo.Producto;
import techu.practitioner.servicio.repos.RepositorioProductoPersonalizado;

import java.util.List;

public class RepositorioProductoPersonalizadoImpl implements RepositorioProductoPersonalizado {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<Producto> buscarPorRangoPrecioComplicado(double min, double max) {

        System.out.println(String.format("===== buscarPorRangoPrecioComplicado(%f, %f)", min, max));

        final Query filtro = new Query();
        filtro.addCriteria(Criteria.where("precio").gte(min).lte(max));

        return mongoTemplate.find(filtro, Producto.class);
    }

    public void emparcharProducto(String idProducto, String nombre, Double precio, Double cantidad) {

        final Query filtro = new Query();
        filtro.addCriteria(Criteria.where("_id").is(new ObjectId(idProducto)));

        final Update parche = new Update();
        if(precio != null) parche.set("precio", precio);
        if(cantidad != null) parche.set("cantidad", cantidad);
        if(nombre != null) parche.set("nombre", nombre);

        this.mongoTemplate.updateFirst(filtro, parche, "producto");
    }

    public boolean existeProducto(String idProducto) {
        final Query filtro = new Query();
        filtro.addCriteria(Criteria.where("_id").is(new ObjectId(idProducto)));

        return this.mongoTemplate.exists(filtro, "producto");
    }
}