package techu.practitioner.servicio.repos;

import org.springframework.data.mongodb.repository.MongoRepository;
import techu.practitioner.modelo.Proveedor;

public interface RepositorioProveedor extends MongoRepository<Proveedor, String>, RepositorioProveedorPersonalizado {
}