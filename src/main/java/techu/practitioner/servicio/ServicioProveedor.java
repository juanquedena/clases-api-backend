package techu.practitioner.servicio;

import techu.practitioner.modelo.DatosPedido;
import techu.practitioner.modelo.Proveedor;

import java.util.List;

public interface ServicioProveedor {
    String agregar(Proveedor p);

    List<Proveedor> obtenerProveedores();

    Proveedor obtenerProveedor(String id);

    void reemplazarProveedor(String id, Proveedor p);

    void borrarProveedor(String id);

    List<DatosPedido> obtenerPedidos(String producto, Integer estado);
}
