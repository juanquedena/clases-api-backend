package techu.practitioner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.ForwardedHeaderFilter;

import java.util.Arrays;

@SpringBootApplication
public class BackendTallerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendTallerApplication.class, args);
	}

	@Bean
	public ForwardedHeaderFilter forwardedHeaderFilter() {
		// X-Forwarded-Proto
		// X-Forwarded-Host
		// X-Forwarded-Port
		return new ForwardedHeaderFilter();
	}

	@Bean
	public CommandLineRunner lineaComando(ApplicationContext app) {
		return args -> {
			final String[] beans = app.getBeanDefinitionNames();
			System.out.println("# Beans: " + app.getBeanDefinitionCount());
			Arrays.sort(beans);
			for(String b: beans) {
				System.out.println("Bean: " + b);
			}
		};
	}
}
